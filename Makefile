
PKG_PATH=org/eustrosoft/pspn/csv/
PKG_SRC_ALL=${PKG_PATH}/*.java
PKG_CLASS_ALL=${PKG_PATH}/*.class
PKG_FILENAME=zCSV-sketch
WORK_PATH=work/
WORKDOC_PATH=${WORK_PATH}/javadoc
INSTALL=install -m 644
RUN_CLASS=org/eustrosoft/pspn/csv/ZCSVFile
JAVAC?=javac
#JAVAC=javac  -Xlint:unchecked
JAVA?=java
JAVADOC?=javadoc -private
JAR?=jar

usage:
	@echo "make (all|build|jar|run|clean)"
all: build jar
test: build run
run:
	java org/eustrosoft/pspn/csv/ZCSVFile
build:
	javac ${PKG_SRC_ALL}
pspn:
	javac org/eustrosoft/pspn/Z*java #  org/eustrosoft/pspn/csv/ZCSV*java
jar: build
	mkdir -p ${WORK_PATH}
	mkdir -p ${WORKDOC_PATH}
	${JAR} -c0f ${WORK_PATH}/${PKG_FILENAME}.jar ${PKG_CLASS_ALL}
clean:
	-rm org/eustrosoft/pspn/csv/ZCSV*.class
	-rm org/eustrosoft/pspn/Z*.class
	-rm -rf ${WORK_PATH}
