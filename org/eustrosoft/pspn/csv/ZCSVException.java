// EustroSoft.org PSPN/CSV project
//
// (c) Alex V Eustrop & EustroSoft.org 2020
// 
// LICENSE: BALES, ISC, MIT, BSD on your choice
//
//

package org.eustrosoft.pspn.csv;

/** exception which this package able to throw.
 */
public class ZCSVException extends Exception
{
public static final String SEE_NESTED_CAUSE="SEE_NESTED_CAUSE";
private String cause_msg;
public String getCauseMsg(){return(cause_msg);}
public boolean isCauseMsg(String cause){if(cause == null || cause_msg == null) return(false); return(cause_msg.equals(cause));}

// constructors
public ZCSVException(){}
public ZCSVException(String s){super(s);cause_msg=s;}
public ZCSVException(Exception e){super(SEE_NESTED_CAUSE,e);cause_msg=SEE_NESTED_CAUSE;}
public ZCSVException(String s, Exception e){super(s + " : " + e.getMessage());cause_msg=s;}

} //ZCSVException
