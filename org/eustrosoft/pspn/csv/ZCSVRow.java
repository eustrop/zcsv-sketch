// EustroSoft.org PSPN/CSV project
//
// (c) Alex V Eustrop & EustroSoft.org 2020
// 
// LICENSE: BALES, ISC, MIT, BSD on your choice
//
//

package org.eustrosoft.pspn.csv;
import java.util.*;

/** single row from CSV file
 */
public class ZCSVRow
{
// row instance data fields
private String row=null;
private String[] fields=null;
private int NF=0; // number of fields fo current row
private String FS=null; // field separator
public String getFS(){return(FS);}
public String getOFS(){return(FS);} //SIC! OFS===FS
/** change row FS. if row was not changed - resplit it with resetFS() (like awk) */
public String setFS(String newFS){ if(row != null) return(resetFS(newFS)); String oldFS=FS; FS=newFS; return(oldFS);
}
public String setOFS(String newFS){String oldFS=FS;FS=newFS;return(oldFS);} //SIC! OFS===FS
/** resplit row with new FS. allways reconstruct it before resplitting with old one */
public String resetFS(String newFS){
 String oldFS=FS;
 set_row(get(0),newFS);
 FS=newFS;
 return(oldFS);
}

public static final String FS_SEMICOLON=";"; //ESCAPED ALWAYS when csv_encode
public static final String FS_COLON=":"; //ESCAPED ALWAYS
public static final String FS_TAB="\t"; //ESCAPED only if it is FS
public static final String FS_CR="\n"; //ESCAPED ALWAYS
public static final String FS_LF="\r"; //ESCAPED ALWAYS
public static final String FS_CRLF="\r\n"; //SIC! special meaning - any sequence of {\n || \r} characters treated as FS
public static final String FS_WSPACE=" \t"; // SIC! special meaning - any sequence of white space characters is FS, can be ESCAPED to seq of "_" & "\\t"
public static final String FS_COMMA=","; // _NEVER_ ESCAPED
public static final String FS_DOT="."; // _NEVER_ ESCAPED
public static final String FS_EQUAL="="; // _NEVER_ ESCAPED
public static final String FS_DEFAULT=FS_SEMICOLON; // it's default for ZCSVFile, not for this class! FS=null by default here
public static final String RS_DEFAULT=FS_CR; // it's default row (record) separator for ZCSVFile, not for this class!

// ENCODE/DECODE rules (unfinished)
// { ";", ":", "\n", "\r", "\.", "\t" } -> { "\.,", "\..","\\n","\\r","\.\.","\\t" }
// { ";", ":", "\n", "\r", "\.", "\t" } -> { "\.,", "\..","\.n","\.r","\.t" }
// { "\.,", "\..","\\n","\\r","\\t" } -> {"\.\.,", "\.\.,","\.\\n","\.\\r"}
    public static String[] CSV_UNSAFE_CHARACTERS_SEMICOLON = {";","\\.,","\n","\\n"}; //!SIC do review! (eustrop)
    public static String[] CSV_UNSAFE_CHARACTERS_SUBST_SEMICOLON = {"\\.,","\\\\.,","\\n","\\\\n"};
    public static String[] CSV_UNSAFE_CHARACTERS_COLON = {":","\\..","\n","\\n"}; //!SIC do review (eustrop)
    public static String[] CSV_UNSAFE_CHARACTERS_SUBST_COLON = {"\\..","\\\\..","\\n","\\\\n"};
    public static String[] CSV_UNSAFE_CHARACTERS_TAB = {"\t","\\t","\n","\\n"}; //!SIC do review (eustrop)
    public static String[] CSV_UNSAFE_CHARACTERS_SUBST_TAB = {"\\t","\\\\t","\\n","\\\\n"};


// from ConcepTIS WAMessages
    /** replace all sz's occurrences of 'from[x]' onto 'to[x]' and return the result.
     * Each occurence processed once and result depend on token's order at 'from'.
     * For instance: translate_tokens("hello",new String[]{"he","hel","hl"}, new String[]{"eh","leh","lh"})
     * give "ehllo", not "lehlo" or "elhlo" (in fact "hel" to "leh" translation never be done).
     */
    public static String translate_tokens(String sz, String[] from, String[] to)
    {
        if(sz == null) return(sz);
        StringBuffer sb = new StringBuffer(sz.length() + 256);
        int p=0;
        while(p<sz.length())
        {
            int i=0;
            while(i<from.length) // search for token
            {
                if(sz.startsWith(from[i],p)) { sb.append(to[i]); p=--p +from[i].length(); break; }
                i++;
            }
            if(i>=from.length) sb.append(sz.charAt(p)); // not found
            p++;
        }
        return(sb.toString());
    } // translate_tokens


private String csv_encode(String f){
 String OFS=getOFS();
 if(FS_SEMICOLON.equals(OFS))
 {
  f=translate_tokens(f,CSV_UNSAFE_CHARACTERS_SEMICOLON,CSV_UNSAFE_CHARACTERS_SUBST_SEMICOLON);
 }
 else if(FS_COLON.equals(OFS))
 {
  f=translate_tokens(f,CSV_UNSAFE_CHARACTERS_COLON,CSV_UNSAFE_CHARACTERS_SUBST_COLON);
 }
 else if(FS_TAB.equals(OFS))
 {
  f=translate_tokens(f,CSV_UNSAFE_CHARACTERS_TAB,CSV_UNSAFE_CHARACTERS_SUBST_TAB);
 }
 //else - do nothing, keep as-is
 return(f);
} //SIC! review it sometime (eustrop)
private String csv_decode(String f){
 String FS=getFS();
 if(FS_SEMICOLON.equals(FS))
 {
  f=translate_tokens(f,CSV_UNSAFE_CHARACTERS_SUBST_SEMICOLON,CSV_UNSAFE_CHARACTERS_SEMICOLON);
 }
 else if(FS_COLON.equals(FS))
 {
  f=translate_tokens(f,CSV_UNSAFE_CHARACTERS_SUBST_COLON,CSV_UNSAFE_CHARACTERS_COLON);
 }
 else if(FS_TAB.equals(FS))
 {
  f=translate_tokens(f,CSV_UNSAFE_CHARACTERS_SUBST_TAB,CSV_UNSAFE_CHARACTERS_TAB);
 }
 //else - do nothing, keep as-is
 return(f);
} //SIC! review it sometime (eusrop)

private ZCSVRow previousRow=null;
private boolean is_ro=false; //read only
private boolean is_dirty=false; //was changed
private String[] name_map=null;

// get field number i from current row (awk/sh like) $(0) - whole line, fields numbered from 1 to NF()
public String getField(int i){return(S(i));}
public String $(int i){return(S(i));}
public String S(int i){if(i==0) return(toString()); if(i<0 || fields == null || i>fields.length ) return(null); return(fields[--i]);}
public String get(int i){ return(S(i)); }
public String set(int i, String nv){
  if(is_ro)return(null);
  is_dirty=true;
  String ov= null;
  if(i==0) // set the whole row & split it to fields with current FS
  {
    ov=toString();
    set_row(nv,getFS());
    return(ov);
  }
  row=null;
  if(fields==null)fields=new String[i];
  if(fields.length<=i)resize_fields(i); //SIC! проверить
  i--;
  ov= fields[i]; //SIC!
  fields[i]=nv;
  return(ov);
 }
public String get(String name){return(get(name2column(name)));}
public String set(String name, String nv){return(set(name2column(name),nv));}
public int name2column(String name){ if(!(name_map==null || name==null)) { for(int i=0;i<name_map.length;i++) {if(name.equals(name_map[i]))return(++i);} }return(-1);}
public int n2c(String name){return(name2column(name));}

public String toString(){ //SIC! needs more work (for edge cases)
if(row!=null) return(row);
String OFS=FS;
if(OFS==null) OFS=FS_SEMICOLON;
if(OFS.length()>1){
 if(OFS.equals(FS_WSPACE)) OFS=" ";
 if(OFS.equals(FS_CRLF)) OFS=RS_DEFAULT;
}
StringBuffer sb = new StringBuffer(); //SIC! set size!
int LAST=NF; LAST--;
for(int i=0;i<=LAST;i++){sb.append(csv_encode(fields[i])); sb.append(OFS);}
//sb.append(csv_encode(fields[LAST]));
row=sb.toString();
return(row);
}
public int NF(){return(NF);} // number of fields
public int size(){return(NF());} // number of fields
public void resize(int newsize){resize_fields(newsize);} //set number of fields


private void resize_fields(int size) //nunber of fields will be in [1,size], size is last valid element
{
 NF=size;
 if(size<=fields.length) return;
 String[] new_fields = new String[size];
 copy_array(fields,new_fields);
 fields=new_fields;
}
private static String[] clone_array(String[] arr)
{
 if(arr==null)return(null);
 String[] new_array=new String[arr.length];
 copy_array(arr,new_array);
 return(new_array);
}
private static int copy_array(String[] fields, String[] new_fields) //copy from fields to new_fields and return number of copied elements
{
 int cpsize=fields.length;
 if(new_fields.length<cpsize) cpsize = new_fields.length;
 for(int i=0;i<cpsize;i++){new_fields[i]=fields[i];}
 return(cpsize);
}

public void setRO(){is_ro=true;}
public boolean isRO(){return(is_ro);}
public ZCSVRow edit() //get this row or its RW copy for editing purpose
{
 if(!isRO()) return(this);
 return(getRWCopy());
}
public ZCSVRow getRWCopy() { ZCSVRow rwrow=new ZCSVRow(fields,name_map,FS); return(rwrow); }

public void setPrevious(ZCSVRow pr){previousRow=pr;}
public ZCSVRow getPrevious(){return(previousRow);}

public void setNames(String names[]){name_map=names;}
public String[] getNames(){return(name_map);}


// constructors
private String[] csv_decode(String[] fields)
{
if(fields == null) return(fields);
for(int i=0;i<fields.length;i++)
{
 fields[i]=csv_decode(fields[i]);
}
return(fields);
}
private void set_row(String row, String _FS){this.row=row;FS=_FS; if(FS==null)return; fields=csv_decode(row.split(FS)); NF=fields.length;};
private void set_row(String[] row, String _FS){this.row=null; FS=(_FS!=null)?_FS:FS_DEFAULT; fields=clone_array(row); NF=fields.length;};

public ZCSVRow(){}
public ZCSVRow(String row){ set_row(row,FS); } // FS==null by default at this moment (see it at the top of this file)
public ZCSVRow(String row,String FS){ set_row(row,FS); }
public ZCSVRow(String[] values){ set_row(values,null); }
public ZCSVRow(String[] values, String OFS){ set_row(values,OFS); }
public ZCSVRow(String[] values,String[] names){ set_row(values,null); name_map=names;}
public ZCSVRow(String[] values,String[] names, String OFS){ set_row(values,OFS); name_map=names;}

} //ZCSVRow
