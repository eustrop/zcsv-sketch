// EustroSoft.org PSPN/CSV project
//
// (c) Alex V Eustrop & EustroSoft.org 2020
// 
// LICENSE: BALES, ISC, MIT, BSD on your choice
//
//

package org.eustrosoft.pspn.csv;
import java.util.*;
import java.io.*;

/** work with File as CSV database
 */
public class ZCSVFile
{

// instance data fields
private String CHROOT=null; //SIC! for future use
private String file_name=null; // used for read if no file_name_in and for write if no file_name_out
private String file_name_in=null; // used for open to read if set, file_name instead
private String file_name_out=null; // used for open to write if set, file_name instead
private Reader rdr=null;
private Writer wrtr=null;
private List rows = null; //see setRowsCache()
private ZCSVRow currentRow = null;
private String next_row = null;
private int NR=0; // number of rows read from current rdr
private String RS=ZCSVRow.RS_DEFAULT; //=FS_CR="\n" row separator
private String FS=ZCSVRow.FS_DEFAULT; //=FS_SEMICOLON=";" // field separator
public String setFS(String newFS){String oldFS=FS;FS=newFS;return(oldFS);} // !SIC to think about redistribution to all rows
public String getFS(){return(FS);}
public static String ROW_COMMENT_MARK="#"; // marker of commented lines
public static String ROW_SHEBANG_MARK="#!"; // marker of instream command (unknown commands are treated as commented lines)
private boolean ignore_comment = false;
private boolean use_shebang = false;
public void setIgnoreComment(boolean b){ignore_comment=b;}
public boolean getIgnoreComment(){return(ignore_comment);}
public void setUseShebang(boolean b){use_shebang=b;}
public boolean getUserShebang(){return(use_shebang);}
private int br_size=DEFAULT_READER_BUFFER_SIZE;

//statics
private static final int DEFAULT_READER_BUFFER_SIZE = 4096; 

public void setFileName(String fn){file_name=fn;}
public String getFileName(){return(file_name);}
public void setFileNameIn(String fn){file_name_in=fn;}
public String getFileNameIn(){if(file_name_in != null)return(file_name_in); return(file_name);}
public void setFileNameOut(String fn){file_name_out=fn;}
public String getFileNameOut(){if(file_name_out != null) return(file_name_out); return(file_name);}
public boolean isInOutDiffer() { if(getFileNameIn() != null) return(!getFileNameIn().equals(getFileNameOut())); return(true); }

public void setReader(Reader rdr){this.rdr=rdr;file_name_in=null;}
public void setWriter(Writer wrtr){this.wrtr=wrtr;file_name_out=null;}

public void setRowsCaching(boolean do_caching){if(!do_caching){rows=null; return;}if(rows==null)rows=new Vector();}
public boolean isRowsCaching(){return(rows!=null);}
//private helpers
private ZCSVRow keep_row(ZCSVRow r){if(isRowsCaching())rows.add(r); NR++; return(r);}
private ZCSVRow string2row(String s_row) {return string2row(s_row,FS); }
private ZCSVRow string2row(String s_row, String FS)
{
 //if(FS == null || "".equals(FS)) FS = ZCSVRow.FS_DEFAULT; //SIC! yadzuka line
 if(s_row==null) return(null);
 if(ROW_COMMENT_MARK != null && s_row.startsWith(ROW_COMMENT_MARK))
 {
  if(ROW_SHEBANG_MARK != null && use_shebang && s_row.startsWith(ROW_SHEBANG_MARK))
  {
   // process shebang commands here and follow on after-if
   if(s_row.startsWith("#!CSV_TAB")) { setFS(ZCSVRow.FS_TAB); }
   if(s_row.startsWith("#!CSV_DATA")) { setFS(ZCSVRow.FS_SEMICOLON); }
   return(new ZCSVRow(s_row.trim())); // return as single-field comment-like row for future processing
  }
  if(ignore_comment) return(null); // SIC! null must be properly processed!
  return(new ZCSVRow(s_row)); // return as single-field row for future processing
 }
 return(new ZCSVRow(s_row,FS));
}

// Exceptions (SIC! see for EXCPN_ over this file and collect them here)
public static final String EXCPN_OPEN_NULL_FILENAME = "EXCPN_OPEN_NULL_FILENAME";
public static final String EXCPN_IO_OPEN = "EXCPN_IO_OPEN";
public static final String EXCPN_IO_OPEN_UNKNOWN_MODE = "EXCPN_IO_OPEN_UNKNOWN_MODE";
public static final String EXCPN_IO_OPEN4WRITE_UNKNOWN_MODE = "EXCPN_IO_OPEN4WRITE_UNKNOWN_MODE";
public static final String EXCPN_OPEN4WRITE_FILENAME_EXISTS = "EXCPN_OPEN4WRITE_FILENAME_EXISTS";
public static final String EXCPN_IO = "EXCPN_IO";
public static final String EXCPN_NOT_IMPLEMENTED = "EXCPN_NOT_IMPLEMENTED";
public static final String EXCPN_IO_APPEND_ONE_ROW = "EXCPN_IO_APPEND_ONE_ROW";

// actions on file
//public static final int OPEN_LOCK_ONLY=1;
public static final int OPEN_READ_ONLY=2;
public static final int OPEN_APPEND_ONLY=3;
public static final int OPEN_READ_APPEND=4;
public static final int OPEN_REWRITE=5;
public static final int OPEN_CREATE_NEW=6;
//public static final int OPEN_RANDOM_ACCESS=7;
private static final int OPEN_MODE_LAST=15;
// write modes
public static final int OPEN4WRITE_APPEND=1 + OPEN_MODE_LAST;
public static final int OPEN4WRITE_CREATE_NEW=3 + OPEN_MODE_LAST;
public static final int OPEN4WRITE_REWRITE=4 + OPEN_MODE_LAST;

//open file for read (or write, or append, or lock)
public void open(int mode) throws ZCSVException
{
 switch(mode){
 case(OPEN_READ_ONLY) :
 	open4read();
	break;
 case(OPEN_APPEND_ONLY) :
 	open4write(OPEN4WRITE_APPEND);
	break;
 case(OPEN_READ_APPEND) :
 	open4read();
 	open4write(OPEN4WRITE_APPEND);
	break;
 case(OPEN_REWRITE) :
 	open4write(OPEN4WRITE_REWRITE);
	break;
 case(OPEN_CREATE_NEW) :
 	open4write(OPEN4WRITE_CREATE_NEW);
	break;
 default:
	throw(new ZCSVException(EXCPN_IO_OPEN_UNKNOWN_MODE));
 }
} // open(int)

public void open4write(int mode) throws ZCSVException
{
 String file_name = getFileNameOut();
 
 if(file_name == null) throw (new ZCSVException(EXCPN_OPEN_NULL_FILENAME));
 try{
 boolean is_append=true;
 switch(mode){
 case(OPEN4WRITE_APPEND) :
        is_append=true;
	break;
 case(OPEN4WRITE_REWRITE) :
	is_append=false;
	//preserve_file_version()
	//f = new(RandomAccessFile(file_name)); f.setLength(0); f.close(); // truncate();
	break;
 case(OPEN4WRITE_CREATE_NEW) :
	is_append=false;
        if( (new File(file_name)).createNewFile() ) throw (new ZCSVException(EXCPN_OPEN4WRITE_FILENAME_EXISTS));
	break;
 default:
	throw(new ZCSVException(EXCPN_IO_OPEN4WRITE_UNKNOWN_MODE));
 }
 OutputStream os = new FileOutputStream(file_name,is_append);
 Writer osw = new OutputStreamWriter(os, java.nio.charset.StandardCharsets.UTF_8);
 osw = new BufferedWriter(osw,br_size); //SIC! may be: get_BRdrIfSo(isr){if(br_size>0)return(new BuferedWriter(isr,br_size));return(isr);}
 wrtr = osw;
 }catch(IOException ioe){ throw (new ZCSVException(EXCPN_IO_OPEN,ioe)); }
} // open4write();
public void open4read() throws ZCSVException
{
 String file_name = getFileNameIn();
 if(file_name == null) throw (new ZCSVException(EXCPN_OPEN_NULL_FILENAME));
 try{
 InputStream is = new FileInputStream(file_name);
 Reader isr = new InputStreamReader(is, java.nio.charset.StandardCharsets.UTF_8);
 isr = new BufferedReader(isr,br_size); //SIC! may be: get_BRdrIfSo(isr){if(br_size>0)return(new BuferedReader(isr,br_size));return(isr);}
 rdr = isr;
 }catch(IOException ioe){ throw (new ZCSVException(EXCPN_IO_OPEN,ioe)); }
} // open4read();
public void open() throws ZCSVException {open(OPEN_READ_ONLY);}
private boolean read_next_row() // result stored to global var next_row (String)
{
StringBuffer sb = new StringBuffer();
try{
int c = rdr.read();
if(c==-1) return(false); // no more data, no more row records
while(c != -1) { if(RS.indexOf(c) != -1) break; sb.append((char)c); c=rdr.read(); }
}catch(IOException ioe){next_row=null;return(false);}
next_row=sb.toString();
return(true);
}
public boolean hasNext(){if(next_row!=null)return(true);return(read_next_row());}
// get next row and make it current
public ZCSVRow next(){if(!hasNext())return(null); currentRow=string2row(next_row); next_row=null; return(keep_row(currentRow));}
public ZCSVRow next(String FS){if(!hasNext())return(null); currentRow=string2row(next_row, FS); next_row=null; return(keep_row(currentRow));}
// get current row
public ZCSVRow get(){return(currentRow);}
// position cursore to row number i
public ZCSVRow seek(int i) throws ZCSVException {throw new ZCSVException(EXCPN_NOT_IMPLEMENTED);}
public boolean seekReader(long i) throws ZCSVException {throw new ZCSVException(EXCPN_NOT_IMPLEMENTED);}
public boolean seekWriter(long i) throws ZCSVException {throw new ZCSVException(EXCPN_NOT_IMPLEMENTED);}
// get field number i from current row (awk/sh like) $(0) - whole line, fields numbered from 1
public String getField(int i){return(S(i));}
public String $(int i){return(S(i));}
public String S(int i){return(get().S(i));}
public int NF(){return(get().NF());} // number-of-fields in ther current row
public int NR(){return(NR);} // number-of-records read from current file/reader
public int size(){if(rows == null) return(0); return(rows.size()); } // real number of rows in memory buffer
 // close file and free it for others
public boolean close(){ if(rdr == null) return(false); try{ rdr.close(); } catch(IOException ioe){ return(false); } rdr=null; return(true); }
public boolean closeWriter(){ if(wrtr == null) return(false); try{ wrtr.close(); } catch(IOException ioe){ return(false); } wrtr=null; return(true); }
 // exclusively lock file (can be used before update)
public boolean lock(){return(false);}

//actions on file content
 // load all lines from file & parse valid rows
public int load() throws ZCSVException {
open();
 setRowsCaching(true);
 while(hasNext()){next();}
close();
return(NR());
}
 // write row to current writer
public void appendOneRow(ZCSVRow row) throws ZCSVException { //append one row
//throw new ZCSVException(EXCPN_NOT_IMPLEMENTED);
open4write(OPEN4WRITE_APPEND);
try{
wrtr.write(row.toString() + "\n");
}catch(IOException ioe){throw new ZCSVException(EXCPN_IO_APPEND_ONE_ROW);}
finally{ closeWriter(); }
}
public void write(ZCSVRow row) throws ZCSVException {throw new ZCSVException(EXCPN_NOT_IMPLEMENTED);}
public void write(int i) throws ZCSVException {if(i<NR()) write(get(--i));}
 //reload data from file if changed
public int reload() throws ZCSVException {return(-1);}
 // update file content based on changes done on rows
public int update() throws ZCSVException {return(-1);}
 // fully rewrite content of file with in-memory data
public void rewrite() throws ZCSVException
{
open(OPEN_REWRITE);
if(true) throw new ZCSVException(EXCPN_NOT_IMPLEMENTED);
close();
}
 // the same as as above but new file only
public void writeNew(String filename) throws ZCSVException{throw new ZCSVException(EXCPN_NOT_IMPLEMENTED);}
 // write changes to file but do not touch any existing data (it's paranodal-safe version of update() method)
public void append() throws ZCSVException {throw new ZCSVException(EXCPN_NOT_IMPLEMENTED);}

// get line from loaded file by number (as is, text upto \n)
public ZCSVRow get(int i){if(i<size()) return((ZCSVRow)rows.get(i)); return(null) ;}
// get read-only row from loaded file by number (only proper rows, not commented lines)
public ZCSVRow getRow(int i){return(get(i));}
// the same as above but ready for update, change it and use update() method of parent ZCSVFile
public ZCSVRow editRow(int i){return(get(i));}


// constructors
public ZCSVFile(){}
public ZCSVFile(String file_name){setFileName(file_name);}

//
// Main() for testing & usage examples
//
// read it to understanding
// run it to testing
// remove it to production using
// rewrite it if your need
//

private static final String TEST_FILE_00README="testdata/00readme";
private static final String TEST_FILE_00_NOTEXISTS="testdata/00_not_exists_file";
private static final String TEST_FILE_01_TEST1="testdata/01_test1.csv";
private static final String TEST_FILE_02_TEST2_OUT="testdata/02_test1.csv.out.tmp";
private static final String TEST_FILE_03_APPEND_ONE_ROW="testdata/05_append_one_row.csv";

private static String println(String s) { System.out.println(s); return(s); }
private static String stage(String s){ if(true) println(s); return(s); }
private static String err_println(String s) { System.err.println(s); return(s); }
private static void debug_println(String s) { if(true) err_println(s); }

public static void main(String[] args) 
{
ZCSVFile f = null;
println("BEGIN TESTING");
String STAGE_INFO=stage("00: inital");

// read csv file and print it content
try{
//
STAGE_INFO=stage("01: 00readme printing");
f = new ZCSVFile(TEST_FILE_00README);
f.open();
 while(f.hasNext()){println(f.next().toString());} //just print all lines of file
f.close();
//
STAGE_INFO=stage("02: not exists try printing");
f.setFileName(TEST_FILE_00_NOTEXISTS);
try{
 f.open();
 throw new ZCSVException(f.getFileName() + " Opened successful... but MUST NOT!");
 } catch(ZCSVException zex){ if(!zex.isCauseMsg(EXCPN_IO_OPEN)) throw(zex); println("Ok! can't open " + f.getFileName()); }
//
STAGE_INFO=stage("03: read test data");
f.setFileName(TEST_FILE_01_TEST1);
f.open();
String[] names = {"id","code","name","name_local","value","desc"};
 while(f.hasNext()){ //print each field 
   ZCSVRow r = f.next(); // get next row
   r.setNames(names);
   println("row:" + r.$(0)); // get full row as-is
   println("id:" + r.$(1)); // get first field
   println("code:" + r.$(2));
   println("name:" + r.get("name"));
   println("name_local:" + f.$(4)); // SIC! using f instead of r
   println("value:" + r.$(5));
   println("desc:" + r.S(6)); // SIC! using S instead of $
   println("$7 (not exists):" + r.S(7));
   println("$777 (not exists):" + f.S(777));
   for(int i=0;i<=r.NF();i++){if(!f.$(i).equals(r.$(i))) throw new ZCSVException("fields values differ! $" + i + " r.$= " + r.$(i) + " f.$=" + f.$(i)); }
 }
f.close();
//
STAGE_INFO=stage("04: read test data and copy each row to new one");
f.setFileNameIn(TEST_FILE_01_TEST1);
f.open();
 while(f.hasNext()){ //print each field 
   ZCSVRow r = f.next(); // get next row
   r.setNames(names);
   ZCSVRow nr=new ZCSVRow();
   nr.setNames(names);
   println("row:" + r.$(0)); // get full row as-is
   println("id:" + r.$(1)); // get first field
   println("id:" + r.$(1)); // get first field
   nr.set(1,r.get(1));
   println("id:" + nr.$(1)); // get first field
   println("code:" + r.$(2));
   nr.set(2,r.get(2));
   println("code:" + nr.$(2));
   println("name:" + r.get("name"));
   nr.set("name",r.get("name"));
   println("name:" + nr.get("name"));
   println("name_local:" + f.$(4)); // SIC! using f instead of r
   nr.set("name",r.get("name"));
   println("value:" + r.$(5));
   nr.set(5,r.get(5));
   println("desc:" + r.S(6)); // SIC! using S instead of $
   nr.set(6,r.get(6));
   println("$7 (not exists):" + r.S(7));
   nr.set(7,r.get(7));
   println("$777 (not exists):" + f.S(777));
   nr.set(777,r.get(777));
   for(int i=0;i<=r.NF();i++){if(!f.$(i).equals(r.$(i))) throw new ZCSVException("fields values differ! $" + i + " r.$= " + r.$(i) + " f.$=" + f.$(i)); }
 }
f.close();
//
STAGE_INFO=stage("05: append one row");
f.setFileNameOut(TEST_FILE_03_APPEND_ONE_ROW);
ZCSVRow aorow = new ZCSVRow();
aorow.set(3,"3");
f.appendOneRow(aorow);
//
STAGE_INFO=stage("06: read test data and write it to new file");
f.setFileNameIn(TEST_FILE_01_TEST1);
f.setFileNameOut(TEST_FILE_02_TEST2_OUT);
f.open();
f.close(); /// the same as f.closeIn(); f.closeOut();

} // try over all tests
catch(Exception e)
{
 err_println("FATAL ERROR! uncatched exception:");
 err_println("STAGE_INFO:" + STAGE_INFO);
 if( f != null) err_println("FILENAME: " + f.getFileName());
 else err_println("no current file (f == null)");
 err_println(e.toString());
 e.printStackTrace();
 err_println("FAILED!");
 return;
}
finally{ if(f != null) f.close(); }
println("OK, EVERITHING PASSED");
} // main()

} //ZCSVFile
