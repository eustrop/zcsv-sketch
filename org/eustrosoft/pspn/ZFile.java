// EustroSoft.org PSPN/CSV project
//
// (c) Alex V Eustrop & EustroSoft.org 2020
// 
// LICENSE: BALES, ISC, MIT, BSD on your choice
//
//

package org.eustrosoft.pspn;
import org.eustrosoft.pspn.csv.*;
import java.util.*;
import java.io.*;

/** work with File as TIS/PSPN Data Object, with :
 * 1) access control (mandatory & discetional)
 * 2) access audit (writing log of users's activity)
 * 3) logical transactions and version control (history slices preservation)
 */
public class ZFile
{

// instance data fields
private ZFile CHROOT=null; //SIC! for future use
private String file_name=null; // used for read if no file_name_in and for write if no file_name_out
private String file_name_i=null; // used for open to write if set, file_name instead
private Writer wrtr=null;

//private helpers

// Exceptions (SIC! see for EXCPN_ over this file and collect them here)
public static final String EXCPN_OPEN_NULL_FILENAME = "EXCPN_OPEN_NULL_FILENAME";
public static final String EXCPN_IO_OPEN = "EXCPN_IO_OPEN:";
public static final String EXCPN_IO = "EXCPN_IO:";
public static final String EXCPN_NOT_IMPLEMENTED = "EXCPN_NOT_IMPLEMENTED";

// access atomic actions on file object
public static final int ACCESS_WRITE = 1;	// write to existing (SIC!) object
public static final int ACCESS_CREATE = 2;	// create new object
public static final int ACCESS_DELETE = 4;	// delete existing object
public static final int ACCESS_READ = 8;	// read current version of object
public static final int ACCESS_LOCK = 16;	// set & remove lock on entre object
public static final int ACCESS_HISTORY = 32;	// read history of object (previous versions)
public static final int ACCESS_CHMETA = 64;	// change meta-information on data object
public static final int ACCESS_EXEC = 128;	// EXEC
public static final int ACCESS_RESERV08 = 256;
public static final int ACCESS_RESERV09 = 512;
public static final int ACCESS_RESERV10 = 1024;
public static final int ACCESS_RESERV11 = 2048;
public static final int ACCESS_RESERV12 = 4096;
public static final int ACCESS_RESERV13 = 8192;
public static final int ACCESS_RESERV14 = 16384;
public static final int ACCESS_RESERV15 = 32768;
//public static final int ACCESS_RESERV16 = 65536;
// composite actions
public static final int ACCESS_EDIT = ACCESS_WRITE + ACCESS_READ;
public static final int ACCESS_EDIT_NEW = ACCESS_WRITE + ACCESS_CREATE;
public static final int ACCESS_REVERT = ACCESS_HISTORY + ACCESS_WRITE;
public static final int ACCESS_DIFF = ACCESS_HISTORY + ACCESS_READ;
public static final int ACCESS_MV_FROM = ACCESS_DELETE + ACCESS_READ;
public static final int ACCESS_MV_TO = ACCESS_CREATE + ACCESS_WRITE;
public static final int ACCESS_RENAME_FROM = ACCESS_DELETE;
public static final int ACCESS_RENAME_TO = ACCESS_CREATE;
public static final int ACCESS_LS = ACCESS_READ;
public static final int ACCESS_CI = ACCESS_WRITE;
public static final int ACCESS_CO = ACCESS_HISTORY + ACCESS_WRITE;
public static final int ACCESS_UNDELETE = ACCESS_HISTORY + ACCESS_CREATE;
public static final int ACCESS_UNRM = ACCESS_UNDELETE;
//private static final int OPEN_MODE_LAST=15;
// write modes

public boolean hasAccess(int access) {return(false);}
private void check_access(int access) throws ZPSPNException{}
private void not_implemented() throws ZPSPNException {throw new ZPSPNException(EXCPN_NOT_IMPLEMENTED); }

// simple non-trasaction read
public RandomAccessFile read() throws ZPSPNException { not_implemented(); return(null); }
public Reader getReader() throws ZPSPNException { not_implemented(); return(null); }
public InputStream getInputStream() throws ZPSPNException { not_implemented(); return(null); }
// simple non-trasaction read historical data
public RandomAccessFile readVersion() throws ZPSPNException { not_implemented(); return(null); }
public InputStream getVersionInputStream() throws ZPSPNException { not_implemented(); return(null); }
public Reader getVersionReader() throws ZPSPNException { not_implemented(); return(null); }
//public Reader getVersionDiff() throws ZPSPNException { not_implemented(); return(null); }
// atomic logical transaction operations
public boolean lock() throws ZPSPNException { not_implemented(); return(false); }
public boolean unlock() throws ZPSPNException { not_implemented(); return(false); }
public void delete() throws ZPSPNException { not_implemented(); }
// composite logical transaction methods
//open file for read (or write, or append, or lock)
public void create() throws ZPSPNException { not_implemented(); }
public void open() throws ZPSPNException { not_implemented(); }
public RandomAccessFile write() throws ZPSPNException { not_implemented(); return(null); }
public InputStream getOpenedInputStream() throws ZPSPNException { not_implemented(); return(null); }
public Reader getOpenedReader() throws ZPSPNException { not_implemented(); return(null); }
public OutputStream getOpenedOutputStream() throws ZPSPNException { not_implemented(); return(null); }
public Writer getOpenedWriter() throws ZPSPNException { not_implemented(); return(null); }
public void doQC() throws ZPSPNException { not_implemented(); }
public void commit() throws ZPSPNException { not_implemented(); }
public void rollback() throws ZPSPNException { not_implemented(); }
// META read/write actions
public String getMeta() throws ZPSPNException { not_implemented(); return(null); }
public void setMeta(String meta) throws ZPSPNException { not_implemented(); }
// atomic 
public void revert(int ver) throws ZPSPNException { not_implemented(); }
// CSV/SQL/PSPN actions
public ZCSVFile select(String query) throws ZPSPNException { not_implemented(); return(null); }
public void update(String query) throws ZPSPNException { not_implemented(); }
public void insert(String query) throws ZPSPNException { not_implemented(); }
public void deleteRows(String query) throws ZPSPNException { not_implemented(); }
// composite actions
public void mv() throws ZPSPNException { not_implemented(); }
public void cp() throws ZPSPNException { not_implemented(); }
public void rename() throws ZPSPNException { not_implemented(); }
public void rm() throws ZPSPNException { not_implemented(); }
public void rmdir() throws ZPSPNException { not_implemented(); }
// sh/rcs paradigm to logical transaction bridge methods
public void ci() throws ZPSPNException { not_implemented(); }
public void co() throws ZPSPNException { not_implemented(); }

// constructors
public ZFile(){}
public ZFile(String file_name){this.file_name=file_name;}

//
// Main() for testing & usage examples
//
// read it to understanding
// run it to testing
// remove it to production using
// rewrite it if your need
//

public static void main(String[] args) 
{
// show how to use this class here
} // main()

} //ZFile
